/**
 * JS function for users file
 */

var $,
    console,
    document,
    location,
    showUser;

function getData() {
    "use strict";
    $.ajax({
        type: "POST",
        url: "panel/userSelector.php",
        data: {
            action: "showUsers"
        },
        success: function (msg) {
            document.getElementById("showUsers").innerHTML = msg;
        },
        error: function () {
            console.log("Connection error");
        }
    });
}

(getData());

$("body").on("click", ".edit_user", function () {
    "use strict";
    var fname,
        lname,
        email,
        status,
        phone;
    fname = $(this).closest("tr").find("td#td_name").attr("data-fname");
    lname = $(this).closest("tr").find("td#td_name").attr("data-lname");
    email = $(this).closest("tr").find("td#td_email").html();
    status = $(this).closest("tr").find("td#td_email").attr("data-status");
    phone = $(this).closest("tr").find("td#td_phone").html();
    document.getElementById("edit_fname").value = fname;
    document.getElementById("edit_lname").value = lname;
    document.getElementById("edit_email").value = email;
    document.getElementById("edit_phone").value = phone;
    if (status === "soldier") {
        document.getElementById("input_client").checked = true;
    } else {
        document.getElementById("input_admin").checked = true;
    }
});

function editUser() {
    "use strict";
    var fname,
        lname,
        email,
        status,
        phone;
    fname = $("#edit_fname").val();
    lname = $("#edit_lname").val();
    email = $("#edit_email").val();
    if ($("#input_client").prop("checked") === true) {
        status = "client";
    } else if ($("#input_admin").prop("checked") === true) {
        status = "admin";
    }
    phone = $("#edit_phone").val();
    if (fname === "" || lname === "" || phone === "" || email === "") {
        $("#settings_failed").removeClass("collapse");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/ControlUser.php",
            dataType: "json",
            data: {
                action: "editUser",
                fname: fname,
                lname: lname,
                email: email,
                status: status,
                phone: phone
            },
            success: function (json) {
                if (json.status === "success") {
                    $("#settings_success").removeClass("collapse");
                    document.getElementById("showUsers").innerHTML = "";
                    getData();
                } else if (json.status === "error") {
                    $("#settings_failed_2").removeClass("collapse");
                }
            }
        });
    }
}

$("#settings_failed, #settings_success, #userRemoved").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
});

$("#settings_failed_2").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
    location.reload();
});

$("body").on("click", ".del_user", function () {
    "use strict";
    var name,
        email;
    name = $(this).closest("tr").find("td#td_name").html();
    email = $(this).closest("tr").find("td#td_email").html();
    $("#deL_user_ask").text(name);
    document.getElementById("del_user_email").value = email;
    return true;
});

function delUser() {
    "use strict";
    var email = $("#del_user_email").val();
    if (email === "") {
        $("#settings_failed").removeClass("collapse");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/ControlUser.php",
            dataType: "json",
            data: {
                action: "delUser",
                email: email
            },
            success: function (json) {
                if (json.status === "success") {
                    $("#userRemoved").removeClass("collapse");
                    document.getElementById("showUsers").innerHTML = "";
                    getData();
                } else if (json.status === "error") {
                    $("#settings_failed_2").removeClass("collapse");
                }
            }
        });
    }
}
