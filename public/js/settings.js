/**
 * JS functions for settings
 */

var $,
    location;

function doNewSettings() { // the function is calling after click the button
    "use strict";
    var fname = $("#fname").val(),
        lname = $("#lname").val(),
        phone = $("#phone").val();
    if (fname === "" || lname === "" || phone === "") {
        $("#settings_failed").removeClass("collapse");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/doNewSettings.php",
            dataType: "json",
            data: {
                fname: fname,
                lname: lname,
                phone: phone
            },
            success: function (json) {
                if (json.status === "success") {
                    $("#settings_success").removeClass("collapse");
                    $("#user_name").html(fname + " " + lname);
                } else if (json.status === 'error') {
                    $("#settings_failed_2").removeClass("collapse");
                }
            }
        });
    }
}

$("#settings_failed, #settings_success").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
});

$("#settings_failed_2").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
    location.reload();
});
