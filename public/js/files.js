/**
 * JS file for showing the files
 */
var $,
    console,
    document;

function showFiles() {
    "use strict";
    $.ajax({
        type: "POST",
        url: "panel/getResData.php",
        data: {
            action: "showFiles"
        },
        success: function (ret) {
            $("#filesTable").html(ret);
        },
        error: function () {
            console.log("Connection error");
        }
    });
}

$(document).ready(function () {
    "use strict";
    showFiles();
});
