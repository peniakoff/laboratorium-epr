/**
 * JS functions for reservations
 */

var $,
    console,
    document,
    window,
    location,
    alert,
    respText,
    t = [],
    day;

function showResState(str) {
    "use strict";
    var xmlhttp,
        tableContent,
        thAction = "",
        tDs = "",
        table = document.getElementById("reservationsTable");
    if (str === "" || str === undefined) {
        document.getElementById("reservationsTable").innerHTML = "";
    } else {
        if (window.XMLHttpRequest) {
            xmlhttp = new window.XMLHttpRequest();
        } else if (window.ActiveXObject) {
            xmlhttp = new window.ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                t = this.responseText;
                if (t !== "") {
                    (function () {
                        var i = 0,
                            j = 1,
                            content,
                            details = {};
                        t = JSON.parse(t);

                        function addDetail(ind, jsonTable) {
                            var m,
                                z,
                                data = [],
                                text = "",
                                k = 0,
                                l;
                            switch (jsonTable) {
                            case 1:
                                m = 3;
                                z = 0;
                                break;
                            case 2:
                                m = 5;
                                z = 1;
                                break;
                            case 3:
                                m = 6;
                                z = 2;
                                break;
                            case 4:
                                m = 7;
                                z = 3;
                                break;
                            }
                            data = JSON.parse(t[ind][m]);
                            l = data.length;
                            details = [
                                ["rodnik", "metal"],
                                ["RT", "LN"],
                                ["X", "Q"],
                                ["0-7000", "0-5000", "2500-3700", "inny"]
                            ];
                            for (k; k < l; k += 1) {
                                if (data[k] === 1) {
                                    if (text !== "") {
                                        text += ', ';
                                    }
                                    text += details[z][k];
                                }
                            }
                            return text;
                        }

                        function detailsContent(index) {
                            var detCont = "";
                            detCont += '<span class="name">oznaczenie próbek:</span> <span id="eMark">' + t[index][1] + '</span><br /><span class="name">wzór badanego związku:</span> <span id="eFormula">' + t[index][2] + '</span><br /><span class="name">informacje o próbce:</span> <span id="eSampleInfo">' + addDetail(index, 1) + '</span>';
                            if (t[index][4] !== "") {
                                detCont += ' (<span id="eSampleInfoText">' + t[index][4] + '</span>)';
                            }
                            detCont += '<br /><span class="name">warunki pomiaru:</span> <span id="eConditions">' + addDetail(index, 2) + '</span><br /><span class="name">pasmo:</span> <span id="eFrequency">' + addDetail(index, 3) + '</span><br /><span class="name">zakres pola:</span> <span id="eRangeField">' + addDetail(index, 4) + '</span>';
                            if (t[index][8] !== "") {
                                detCont += ' (<span id="eRangeFieldText">' + t[index][8] + '</span>)';
                            }
                            if (t[index][9] !== "") {
                                detCont += '<br /><span class="name">uwagi:</span> <span id="eResMore">' + t[index][9] + '</span>';
                            }
                            return detCont;
                        }

                        while (t[i]) {
                            content = detailsContent(i);
                            if (str === 1 || str === 2) {
                                tDs += '<tr><td>' + j + '</td><td id="td_res_date">' + t[i][0] + '<br />(' + t[i][11] + ')</td><td id="td_res_details">' + content + '</td><td><div><a href="#" data-toggle="modal" data-target="#edit_res" data-id="' + i + '" id="' + t[i][10] + '" class="edit_res_link" title="edytuj szczegóły rezerwacji">edytuj</a></div><div><a href="#" data-toggle="modal" data-target="#del_res" data-id="' + i + '" id="' + t[i][10] + '" class="del_res_link" title="usuń rezerwację">usuń</a></div></td></tr>';
                            } else if (str === 3) {
                                tDs += '<tr><td>' + j + '</td><td>' + t[i][0] + '</td><td id="td_res_details">' + content + '</td></tr>';
                            }
                            i += 1;
                            j += 1;
                        }
                        if (str === 1 || str === 2) {
                            thAction = '<th style="width: 11%;">Akcje</th>';
                        }
                        tableContent = '<table id="reservations" class="table table-striped table-bordered"><tr><th style="width: 6%;">lp.</th><th style="width: 17%;">Data badania</th><th>Szczegóły</th>' + thAction + '</tr>' + tDs + '</table>';
                        table.innerHTML = tableContent;
                    }());
                } else {
                    table.innerHTML = '<div class="alert alert-info" role="alert"><span class="glyphicon glyphicon-info-sign"></span> Nie masz żadnych rezerwacji o tym statusie!</div>';
                }
            } else if (this.readyState === 0 || this.status === 404) {
                table.innerHTML = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;<strong>Wystąpił błąd w połączeniu!</strong> Spróbuj ponownie lub skonsultuj się z administratorem systemu.</div>';
            }
        };
        xmlhttp.open("GET", "panel/getResData.php?q=" + str, true);
        xmlhttp.send();
    }
    $("button.active").removeClass("active");
}

function myDateFunction(id) {
    "use strict";
    var date = $("#" + id).data("date");
    document.getElementById("res_date").value = date;
    return true;
}

function myDateFunctionEdit(id) {
    "use strict";
    var date = $("#" + id).data("date");
    document.getElementById("editResDate").value = date;
    return true;
}

function newMainCalendar() {
    "use strict";
    $(".calendar-content").empty();
    $(".calendar-content").html("<div id='my-calendar'></div>");
    $("#my-calendar").zabuto_calendar({
        action: function () {
            return myDateFunctionEdit(this.id, false);
        }
    });
}

function getDayOfWeek() {
    "use strict";
    var dayOfWeek = $("div.badge-checked").parent().index();
    switch (dayOfWeek) {
    case 0:
        dayOfWeek = "poniedziałek";
        break;
    case 1:
        dayOfWeek = "wtorek";
        break;
    case 2:
        dayOfWeek = "środa";
        break;
    case 3:
        dayOfWeek = "czwartek";
        break;
    case 4:
        dayOfWeek = "piątek";
        break;
    case 5:
        dayOfWeek = "sobota";
        break;
    }
    return dayOfWeek;
}

function doReservation() {
    "use strict";
    var sampleInfo = [],
        conditions = [],
        freq = [],
        rangeField = [],
        reservationData = {},
        functionArg = [
            ["sampleInfo", "conditions", "freq", "rangeField"],
            [sampleInfo, conditions, freq, rangeField]
        ],
        info,
        i = 0;

    function funcArg() {
        var j = 0,
            sample = [];
        while (info[j]) {
            if (info[j].checked === true) {
                sample[j] = 1;
            } else {
                sample[j] = 0;
            }
            j += 1;
        }
        return sample;
    }

    while (functionArg[0][i]) {
        info = document.getElementsByName(functionArg[0][i]);
        functionArg[1][i] = funcArg();
        i += 1;
    }

    reservationData = {
        date: $("#res_date").val(),
        mark: $("textarea#mark").val(),
        formula: $("textarea#formula").val(),
        sampleInfo: functionArg[1][0],
        sampleInfoText: $("#sampleInfoText").val(),
        conditions: functionArg[1][1],
        freq: functionArg[1][2],
        rangeField: functionArg[1][3],
        rangeFieldText: $("#rangeFieldText").val(),
        comments: $("textarea#res_more").val(),
        day: getDayOfWeek()
    };

    if ((reservationData.date === "")
            || (reservationData.date === undefined)
            || (reservationData.mark === "")
            || (reservationData.formula === "")
            || ((reservationData.sampleInfo[0] === 0) && (reservationData.sampleInfo[1] === 0))
            || ((reservationData.conditions[0] === 0) && (reservationData.conditions[1] === 0))
            || ((reservationData.freq[0] === 0) && (reservationData.freq[1] === 0))
            || ((reservationData.rangeField[0] === 0) && (reservationData.rangeField[1] === 0) && (reservationData.rangeField[2] === 0) && (reservationData.rangeField[3] === 0))) {
        (function () {
            var top = document.getElementById("ReservationContent").clientHeight;
            $("#reservation_failed").removeClass("collapse");
            $("body, html").animate({scrollTop: top}, 800);
        }());
    } else if ((reservationData.sampleInfo[1] === 1) && (reservationData.sampleInfoText === "")) {
        $("#sampleInfoText").popover("show");
    } else if ((reservationData.rangeField[3] === 1) && (reservationData.rangeFieldText === "")) {
        $("#rangeFieldText").popover("show");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/ControlReservations.php",
            dataType: "json",
            data: {
                action: "doReservation",
                res_date: reservationData.date,
                mark: reservationData.mark,
                formula: reservationData.formula,
                sampleInfo: JSON.stringify(reservationData.sampleInfo),
                sampleInfoText: reservationData.sampleInfoText,
                conditions: JSON.stringify(reservationData.conditions),
                frequency: JSON.stringify(reservationData.freq),
                rangeField: JSON.stringify(reservationData.rangeField),
                rangeFieldText: reservationData.rangeFieldText,
                res_more: reservationData.comments,
                day: reservationData.day
            },
            success: function (json) {
                if (json.status === "success") {
                    newMainCalendar();
                    $("button.active").removeClass("active");
                    showResState("");
                    $("#reservation_success").removeClass("collapse");
                    $("#reservationsContent").collapse("hide");
                    $("#res_date, textarea#mark, textarea#formula, #sampleInfoText, #rangeFieldText, textarea#res_more").val("");
                    $("input[name=sampleInfo]:checked, input[name=conditions]:checked, input[name=freq]:checked, input[name=rangeField]:checked").prop("checked", false);
                } else if (json.status === "error") {
                    (function () {
                        var top = document.getElementById("ReservationContent").clientHeight;
                        $("#reservation_failed_2").removeClass("collapse");
                        $("body, html").animate({scrollTop: top}, 800);
                    }());
                    console.log(json.error);
                }
            }
        });
    }
}

$("#reservation_failed, #reservation_success, #res_edit_failed, #res_del_success").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
});

$("#reservation_failed_2").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
    location.reload();
});

$("#res_edit_failed_2").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
    $(".calendar-edit-content").empty();
    $(".calendar-edit-content").html('<div id="my-calendar-edit"></div>');
    $("#my-calendar-edit").zabuto_calendar({
        action: function () {
            return myDateFunctionEdit(this.id, false);
        }
    });
});

$("body").on("click", ".edit_res_link", function () { //action after click the "edit" link
    "use strict";
    var a = this.getAttribute("data-id"),
        i,
        k = 0,
        details = [
            [3, 5, 6, 7],
            ["editSampleInfo", "editConditions", "editFrequency", "editRangeField"],
            ["rodnik", "metal"],
            ["RT", "LN"],
            ["X", "Q"],
            ["0-7000", "0-5000", "2500-3700", "inny"]
        ],
        di = 0,
        dIt,
        dItI,
        z,
        zName,
        zTable,
        zLength;

    while (details[0][di]) {
        dIt = document.getElementsByName(details[1][di]);
        for (dItI = 0; dItI < dIt.length; dItI += 1) {
            dIt[dItI].checked = false;
        }
        di += 1;
    }

    $(".calendar-content-edit").html('<div id="my-calendar-edit"></div>');
    $("#my-calendar-edit").zabuto_calendar({
        action: function () {
            return myDateFunctionEdit(this.id, false);
        }
    });

    document.getElementById("curDateEdit").value = t[a][0];
    document.getElementById("editResID").value = t[a][10];
    document.getElementById("editMark").value = t[a][1];
    document.getElementById("editFormula").value = t[a][2];
    document.getElementById("editRangeFieldText").value = t[a][8];
    document.getElementById("editResMore").value = t[a][9];
    document.getElementById("editSampleInfoText").value = t[a][4];
    day = t[a][11];

    while (details[1][k]) {
        z = "";
        zName = "";
        zTable = [];
        z = details[0][k];
        zName = details[1][k];
        zTable = JSON.parse(t[a][z]);
        zLength = zTable.length;
        for (i = 0; i < zLength; i += 1) {
            if (zTable[i] === 1) {
                document.getElementsByName(zName)[i].checked = true;
            }
        }
        k += 1;
    }
});

function editReservation() {
    "use strict";
    var date = $("#editResDate").val(),
        sampleInfo = [],
        conditions = [],
        freq = [],
        rangeField = [],
        reservationData = {},
        functionArg = [
            ["editSampleInfo", "editConditions", "editFrequency", "editRangeField"],
            [sampleInfo, conditions, freq, rangeField]
        ],
        info,
        i = 0,
        dayOfWeek;

    if ((date === "") || (date === undefined)) {
        dayOfWeek = day;
    } else {
        dayOfWeek = getDayOfWeek();
    }

    function funcArg() {
        var j = 0,
            sample = [];
        while (info[j]) {
            if (info[j].checked === true) {
                sample[j] = 1;
            } else {
                sample[j] = 0;
            }
            j += 1;
        }
        return sample;
    }

    while (functionArg[0][i]) {
        info = document.getElementsByName(functionArg[0][i]);
        functionArg[1][i] = funcArg();
        i += 1;
    }

    reservationData = {
        curDateEdit: $("#curDateEdit").val(),
        editResID: $("#editResID").val(),
        date: date,
        mark: $("textarea#editMark").val(),
        formula: $("textarea#editFormula").val(),
        sampleInfo: functionArg[1][0],
        sampleInfoText: $("#editSampleInfoText").val(),
        conditions: functionArg[1][1],
        freq: functionArg[1][2],
        rangeField: functionArg[1][3],
        rangeFieldText: $("#editRangeFieldText").val(),
        comments: $("textarea#editResMore").val(),
        day: dayOfWeek
    };

    if ((reservationData.mark === "")
            || (reservationData.formula === "")
            || ((reservationData.sampleInfo[0] === 0) && (reservationData.sampleInfo[1] === 0))
            || ((reservationData.conditions[0] === 0) && (reservationData.conditions[1] === 0))
            || ((reservationData.freq[0] === 0) && (reservationData.freq[1] === 0))
            || ((reservationData.rangeField[0] === 0) && (reservationData.rangeField[1] === 0) && (reservationData.rangeField[2] === 0) && (reservationData.rangeField[3] === 0))) {
        (function () {
            $("#res_edit_failed").removeClass("collapse");
            $("body, html, div").animate({scrollTop: 0}, 800);
        }());
    } else if ((reservationData.sampleInfo[1] === 1) && (reservationData.sampleInfoText === "")) {
        $("#editSampleInfoText").popover("show");
    } else if ((reservationData.rangeField[3] === 1) && (reservationData.rangeFieldText === "")) {
        $("#editRangeFieldText").popover("show");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/ControlReservations.php",
            dataType: "json",
            data: {
                action: "editReservation",
                curDateEdit: reservationData.curDateEdit,
                editResID: reservationData.editResID,
                editResDate: reservationData.date,
                mark: reservationData.mark,
                formula: reservationData.formula,
                sampleInfo: JSON.stringify(reservationData.sampleInfo),
                sampleInfoText: reservationData.sampleInfoText,
                conditions: JSON.stringify(reservationData.conditions),
                frequency: JSON.stringify(reservationData.freq),
                rangeField: JSON.stringify(reservationData.rangeField),
                rangeFieldText: reservationData.rangeFieldText,
                res_more: reservationData.comments,
                day: reservationData.day
            },
            success: function (json) {
                if (json.status === "success") {
                    $("#edit_res").modal("hide");
                    $("button.active").removeClass("active");
                    showResState("");
                    $("#reservation_success").removeClass("collapse");
                    $(".calendar-content-edit").empty();
                    newMainCalendar();
                } else if (json.status === "error") {
                    (function () {
                        $("#res_edit_failed_2").removeClass("collapse");
                        $("body, html, div").animate({scrollTop: 0}, 800);
                    }());
                    console.log(json.error);
                }
            }
        });
        return false;
    }
}

$("body").on("click", ".del_res_link", function () { //action after click the "deleting" link
    "use strict";
    var res_id = $(this).attr("id"),
        a = this.getAttribute("data-id");
    document.getElementById("deL_date_ask").innerHTML = t[a][0];
    document.getElementById("del_res_date").value = t[a][0];
    document.getElementById("del_res_id").value = res_id;
});

function delReservation() {
    "use strict";
    var del_res_date = $("#del_res_date").val(),
        del_res_id = $("#del_res_id").val();
    if (del_res_date === null || del_res_date === undefined || del_res_id === null || del_res_id === undefined) {
        $("#del_res").modal("hide");
        $("#reservation_failed").removeClass("collapse");
    } else {
        $.ajax({
            type: "POST",
            url: "panel/ControlReservations.php",
            dataType: "json",
            data: {
                action: "delReservation",
                del_res_date: del_res_date,
                del_res_id: del_res_id
            },
            success: function (json) {
                if (json.status === "success") {
                    $("#del_res").modal("hide");
                    $(".calendar-content").empty();
                    $(".calendar-content").html('<div id="my-calendar"></div>');
                    $("#my-calendar").zabuto_calendar({
                        action: function () {
                            return myDateFunction(this.id, false);
                        }
                    });
                    $("button.active").removeClass("active");
                    showResState("");
                    $("#res_del_success").removeClass("collapse");
                } else if (json.status === "error") {
                    $("#reservation_failed_2").removeClass("collapse");
                    console.log(json.error);
                }
            }
        });
    }
}

$(document).ready(function () {
    "use strict";
    $("#my-calendar").zabuto_calendar({ //initialize the calendar
        action: function () {
            return myDateFunction(this.id, false);
        }
    });
    $("#edit_res").on("hide.bs.modal", function () { //initialize the "edit" calendar
        $(".calendar-edit-content").empty();
        $(".calendar-edit-content").html('<div id="my-calendar-edit"></div>');
    });
    $("#reservationsContent").on("show.bs.collapse", function () {
        document.getElementById("listDown").className = "glyphicon glyphicon-collapse-up";
    });
    $("#reservationsContent").on("hide.bs.collapse", function () {
        document.getElementById("listDown").className = "glyphicon glyphicon-collapse-down";
    });
});

$(window).load(function () {
    "use strict";
    $("body").on("click", ".zabuto_calendar .table tr td.dow-clickable div.day", function () {
        $(".badge-checked").removeClass("badge-checked");
        $(this).addClass("badge-checked");
    });
    $("body").on("mouseenter", ".zabuto_calendar", function () {
        $(".zabuto_calendar .table td:nth-child(6), .zabuto_calendar .table td:nth-child(7)").removeClass("dow-clickable").off();
        $(".zabuto_calendar .table td:nth-child(6), .zabuto_calendar .table td:nth-child(7)").css("cursor", "default");
        $(".zabuto_calendar .table tr td.near").off();
    });
    $(".zabuto_calendar .table td:nth-child(6), .zabuto_calendar .table td:nth-child(7)").removeClass("dow-clickable").off();
    $(".zabuto_calendar .table td:nth-child(6), .zabuto_calendar .table td:nth-child(7)").css("cursor", "default");
    $(".zabuto_calendar .table tr td.near").off();
});

function hidePopover(id) {
    "use strict";
    $(id).popover("hide");
}

function thePast(id) {
    "use strict";
    var d = new Date(),
        month = d.getMonth() + 1,
        day = d.getDate(),
        fullDate,
        date = document.getElementById("res_date").value;
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    fullDate = d.getFullYear() + "-" + month + "-" + day;
    if ((date !== "") && (date < fullDate)) {
        $("#calendar-content").popover("show");
    } else {
        $(id).popover("hide");
    }
}
