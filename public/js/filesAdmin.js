/**
 * JS file for manage the files for reservations
 */
var res_id_file,
    user_file,
    $,
    console,
    document,
    alert,
    FormData;

function showFiles(res_id_file, user_file) {
    "use strict";
    $.ajax({
        type: "POST",
        url: "panel/getResData.php",
        data: {
            action: 'showFiles',
            res_id: res_id_file,
            user: user_file
        },
        success: function (ret) {
            $('#filesTable').html(ret);
        },
        error: function () {
            console.log('Connection error');
        }
    });
}

$('body').on('click', 'a#del_file', function (e) {
    "use strict";
    e.preventDefault();
    var fileName = $(this).closest('tr').find('a#down_file').html(),
        fileID = $(this).attr('data-id');

    $.ajax({
        type : "POST",
        url : "panel/upload.php",
        dataType : 'json',
        data : {
            action : 'delFile',
            fileName : fileName,
            fileID : fileID
        },
        success: function (json) {
            if (json.status === 'success') {
                var user_file = $('input#user_file').val(),
                    res_id_file = $('input#res_id_file').val();
                showFiles(res_id_file, user_file);
                $("#delFileSuccess").removeClass('collapse');
            } else if (json.status === 'error') {
                $("#delFileFiled").removeClass('collapse');
            }
        },
        error: function () {
            console.log('Connection error!');
        }
    });
});

$("#delFileSuccess, #delFileFailed").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
});

$('body').on('click', '.manage_file_link', function () {
    "use strict";
    var user_file = $(this).closest('tr').find('td#td_user').attr('date-email'),
        res_id_file = $(this).attr('id');
    document.getElementById('user_file').value = user_file;
    document.getElementById('res_id_file').value = res_id_file;
    showFiles(res_id_file, user_file);
    return true;
});

/* upload file function */

$("#upload_file").on('submit', function (e) {
    "use strict";
    e.preventDefault();
    var user = $('input#user_file').val(),
        res_id = $('input#res_id_file').val();
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: "panel/upload.php",
        data: new FormData(this), //data from file form
        dataType: 'json',
        contentType: false,
        cache: false,
        processData: false,
        success: function (json) {
            if (json.status === 'success') {
                var finalPath = json.finalPath;
                $.ajax({
                    type: "POST",
                    url: "panel/upload.php",
                    dataType : 'json',
                    data: {
                        action: 'file_saved',
                        finalPath: finalPath,
                        user: user,
                        res_id: res_id
                    },
                    success: function (json) {
                        if (json.status === 'success') {
                            var res_id_file = res_id,
                                user_file = user;
                            showFiles(res_id_file, user_file);
                            $('#loader').hide();
                            $("#sendFileSuccess").removeClass('collapse');
                            $('input#file').val('');
                        } else if (json.status === 'error') {
                            $('#loader').hide();
                            $('#glyphiconSubmit').show();
                            $("#sendFileFailed").removeClass('collapse');
                            $('input#file').val('');
                        }
                    }
                });
            } else if (json.status === 'error') {
                $('#loader').hide();
                alert('Operacja nie powiodła się! Sprawdź nazwę przesyłanego pliku!');
                $('input#file').val('');
            }
        },
        complete: function () {
            $("#loader").hide();
        },
        error: function () {
            console.log('Connection error!');
        }
    });
});

$("#sendFileSuccess, #sendFileFailed").on("click", "button", function () {
    "use strict";
    $(this).parent().addClass("collapse");
});

$('#files_modal').on('hide.bs.modal', function () {
    "use strict";
    $("#sendFileSuccess, #sendFileFailed, #delFileSuccess, #delFileFailed").addClass("collapse");
});
