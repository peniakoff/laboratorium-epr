<?php

Route::get('/', 'PagesController@home');
Route::get('wyposazenie', 'PagesController@eqipment');
Route::get('regulamin', 'PagesController@terms');
Route::get('kontakt', 'PagesController@contact');
Route::get('dydaktyka', 'PagesController@education');
