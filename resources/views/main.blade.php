<!DOCTYPE HTML>
<html lang="pl">

</html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>EPR</title>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/calendar.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/theme.css') }}" />
    </head>

    <body >
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container" style="padding: 0;">
           <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
             </button>
             <!--<a class="navbar-brand" href="http://uni.wroc.pl" title="Uniwersytet Wrocławski"><img src="{{ URL::asset('img/uwr_logo_small.png') }}" /></a>-->
           </div>
           <div id="navbar" class="collapse navbar-collapse">
           <ul class="nav navbar-nav">
              <li><a href="/" title="Laboratorium EPR Wydziału Chemii - strona główna">Laboratorium EPR</a></li>
            </ul>
           <ul class="nav navbar-nav navbar-right">
              <li><a href="/wyposazenie" title="Laboratorium EPR Wydziału Chemii - wyposażenie">Wyposażenie</a></li>
              <li><a href="/regulamin" title="Laboratorium EPR Wydziału Chemii - regulamin">Regulamin</a></li>
              <li><a href="/kontakt" title="Laboratorium EPR Wydziału Chemii - kontakt">Kontakt</a></li>
              <li><a href="/dydaktyka" title="Laboratorium EPR Wydziału Chemii - materiały dydaktyczne">Dydaktyka</a></li>
              <?php
              if (!isset($_SESSION['logged'])) {
                 if (isset($_SESSION['no_data'])) {
                    echo ('<li><a href="/dane"><span class="glyphicon glyphicon-user"></span>&nbsp;Panel użytkownika</a></li>');
                 }
                 else {
                    echo ('<li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Panel użytkownika</a></li>');
                 }
              }
              if ((isset($_SESSION['logged'])) && ($_SESSION['logged'] == true)) echo ('<li><a href="/panel"><span class="glyphicon glyphicon-user"></span>&nbsp;<span id="user_name">'.$_SESSION['user'].'</span></a></li>');
              ?>
           </ul>
           </div>
          </div>
        </nav>
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login">
          <div class="modal-dialog" role="document">
           <div class="modal-content">
             <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Zaloguj się do panelu użytkownika</h4>
             </div>
             <div class="modal-body">
                  <?php
                    if ((isset($_SESSION['error'])) && ($_SESSION['error'] == true)) {
                       echo ('<div class="alert alert-danger alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <strong>Podane hasło lub login są niepoprawne!</strong> Spróbuj zalogować się ponownie.
                       </div>');
                    }
                    if ((isset($_SESSION['new_user'])) && ($_SESSION['new_user'] == true)) {
                       echo ('<div class="alert alert-success alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <strong>Twoje konto zostało utworzone!</strong> Zaloguj się, aby wykorzystać swoje konto.
                       </div>');
                    }
                 ?>
                <form class="form-signin" action="<?php //echo ($server.'sub/zaloguj.php'); ?>" method="post">
                 <label class="sr-only">Adres e-mail</label>
                 <input name="email" type="email" id="emailaddress" class="emailaddress form-control" placeholder="Adres e-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required autofocus autocomplete="on" />
                 <label class="sr-only">Hasło</label>
                 <input name="pass" type="password" id="inputPassword" class="form-control" placeholder="Hasło" required />
                 <input class="btn btn-lg btn-primary btn-block" type="submit" value="Zaloguj" />
                 <a href="#" class="btn btn-default btn-block" style="display: none;">Nie pamiętam hasła</a>
                 <a href="<?php //echo ($server.'sub/rejestracja.php'); ?>" class="btn btn-default btn-block">Zarejestruj się</a>
              </form>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
             </div>
           </div>
          </div>
        </div>
        <div id="flexbox">

            @yield('content')

            <footer class="footer">
                <div class="container">
                    <div style="padding: 20px 15px 15px; border-width: 0px 0px 1px; border-style: solid; border-color: rgb(221, 228, 230);">
                        <a href="/">Strona główna</a> | <a href="/wyposazenie">Wyposażenie</a> | <a href="/regulamin">Regulamin</a> | <a href="/kontakt">Kontakt</a> | <?php
                        if (!isset($_SESSION['logged'])) {
                            echo ('<a href="#" data-toggle="modal" data-target="#login">Panel użytkownika</a>');
                        }
                        if ((isset($_SESSION['logged'])) && ($_SESSION['logged'] == true)) {
                            echo ('<a href="/panel">Panel użytkownika</a>');
                        }
                        echo ' | ';
                        if ((!isset($_SESSION['rank'])) || ($_SESSION['rank'] != 'officer')) {
                            echo ('<a href="mailto:tomasz.miller@chem.uni.wroc.pl">Administrator</a>');
                        } elseif ($_SESSION['rank'] == 'officer') {
                            echo ('<a href="/admin/panel" style="color: #2C3E50;">Panel administratora</a>');
                        }
                        ?>
                    </div>
                    <div style="padding: 15px;">
                        <span class="glyphicon glyphicon-copyright-mark"></span>&nbsp;Laboratorium EPR <a href="http://www.chem.uni.wroc.pl">Wydziału Chemii</a> na <a href="http://www.uni.wroc.pl"> Uniwersytecie Wrocławskim.</a> Wrocław&nbsp;<?php if (date("Y") > 2016) echo ('2016-'.date("Y")); else echo ('2016'); ?>
                    </div>
                </div>
            </footer>
        </div>

        <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <?php /*
        if (($id == 4) || ($id == 7)) {
            echo '<script src="https://www.google.com/recaptcha/api.js"></script>';
        }
        if (($id == 5) && ($_GET['p'] == 1)) echo ('
            <script src="'.$server.'js/calendar.min.js"></script>
            <script src="'.$server.'js/reservations.js"></script>
        ');
        if (($id == 5) && ($_GET['p'] == 2)) echo ('<script src="'.$server.'js/files.js"></script>');
        if (($id == 5) && ($_GET['p'] == 3)) echo ('<script src="'.$server.'js/settings.js"></script>');
        if (($id == 8) && ($_GET['p'] == 1)) echo ('<script src="'.$server.'js/users.js"></script>');
        if (($id == 8) && ($_GET['p'] == 2)) echo ('
            <script src="'.$server.'js/calendar.min.js"></script>
            <script src="'.$server.'js/reservationsAdmin.js"></script>
            <script src="'.$server.'js/filesAdmin.js"></script>
        '); */
        ?>
        <script>
            $("#login").on("shown.bs.modal", function () {
                $("#emailaddress").focus()
            });
            <?php /*
            if ((isset($_SESSION['error'])) && ($_SESSION['error'] == true)) {
                echo ('$("#login").modal("toggle");');
                unset($_SESSION['error']);
            }
            if ((isset($_SESSION['new_user'])) && ($_SESSION['new_user'] == true)) {
                echo ('$("#login").modal("toggle");');
                unset($_SESSION['new_user']);
            } */
            ?>
        </script>
    </body>

</html>
