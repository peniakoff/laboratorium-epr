<!DOCTYPE HTML>
<html lang="pl">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php echo ($title_view); ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo ($server.'img/favicon.ico'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo ($server.'css/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo ($server.'css/theme.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo ($server.'css/calendar.css'); ?>" />
</head>
<body >
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container" style="padding: 0;">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="http://uni.wroc.pl" title="Uniwersytet Wrocławski"><img src="<?php echo ($server.'img/uwr_logo_small.png'); ?>" /></a>
	</div>
	<div id="navbar" class="collapse navbar-collapse">
	  <ul class="nav navbar-nav">
		<li><a href="<?php echo ($server); ?>" title="Laboratorium EPR Wydziału Chemii - strona główna">Laboratorium EPR</a></li>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
		<li><a href="<?php echo ($server.'sub/wyposazenie.php'); ?>" title="Laboratorium EPR Wydziału Chemii - wyposażenie">Wyposażenie</a></li>
		<li><a href="<?php echo ($server.'sub/regulamin.php'); ?>" title="Laboratorium EPR Wydziału Chemii - regulamin">Regulamin</a></li>
		<li><a href="<?php echo ($server.'sub/kontakt.php'); ?>" title="Laboratorium EPR Wydziału Chemii - kontakt">Kontakt</a></li>
		<li><a href="<?php echo ($server.'sub/dydaktyka.php'); ?>" title="Laboratorium EPR Wydziału Chemii - materiały dydaktyczne">Dydaktyka</a></li>
		<?php
		if (!isset($_SESSION['logged'])) {
			if (isset($_SESSION['no_data'])) {
				echo ('<li><a href="'.$server.'sub/dane.php"><span class="glyphicon glyphicon-user"></span>&nbsp;Panel użytkownika</a></li>');
			}
			else {
				echo ('<li><a href="#" data-toggle="modal" data-target="#login"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Panel użytkownika</a></li>');
			}
		}
		if ((isset($_SESSION['logged'])) && ($_SESSION['logged'] == true)) echo ('<li><a href="'.$server.'sub/panel.php"><span class="glyphicon glyphicon-user"></span>&nbsp;<span id="user_name">'.$_SESSION['user'].'</span></a></li>');
		?>
	  </ul>
	</div>
  </div>
</nav>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Zaloguj się do panelu użytkownika</h4>
	  </div>
	  <div class="modal-body">
		 	<?php
				if ((isset($_SESSION['error'])) && ($_SESSION['error'] == true)) {
					echo ('<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Podane hasło lub login są niepoprawne!</strong> Spróbuj zalogować się ponownie.
					</div>');
				}
				if ((isset($_SESSION['new_user'])) && ($_SESSION['new_user'] == true)) {
					echo ('<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Twoje konto zostało utworzone!</strong> Zaloguj się, aby wykorzystać swoje konto.
					</div>');
				}
			?>
	  	<form class="form-signin" action="<?php echo ($server.'sub/zaloguj.php'); ?>" method="post">
			<label class="sr-only">Adres e-mail</label>
			<input name="email" type="email" id="emailaddress" class="emailaddress form-control" placeholder="Adres e-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required autofocus autocomplete="on" />
			<label class="sr-only">Hasło</label>
			<input name="pass" type="password" id="inputPassword" class="form-control" placeholder="Hasło" required />
			<input class="btn btn-lg btn-primary btn-block" type="submit" value="Zaloguj" />
			<a href="#" class="btn btn-default btn-block" style="display: none;">Nie pamiętam hasła</a>
			<a href="<?php echo ($server.'sub/rejestracja.php'); ?>" class="btn btn-default btn-block">Zarejestruj się</a>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
	  </div>
	</div>
  </div>
</div>
<!-- start of #flexbox -->
<div id="flexbox">
