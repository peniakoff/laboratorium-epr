@extends('main')
@section('content')

    <div class="jumbotron">
        <div class="container" style="text-align: justify;">
            <h1>Laboratorium EPR</h1>
            <div class="row hidden-xs" style="margin: 20px auto;">
                <div class="col-md-4">
                    <a href="#" class="thumbnail" data-toggle="modal" data-target="#modalp1"><img src="{{ URL::asset('img/info/spin_probe.png') }}" alt="EPR-1" /></a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="thumbnail" data-toggle="modal" data-target="#modalp2"><img src="{{ URL::asset('img/info/widma_2.png') }}" alt="EPR-2" /></a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="thumbnail" data-toggle="modal" data-target="#modalp3"><img src="{{ URL::asset('img/info/widmo_1.png') }}" alt="EPR-3" /></a>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 hidden-xs" style="text-align: center;">
                <a class="btn btn-primary" role="button" href="#" data-toggle="modal" data-target="#modalprobki" style="margin: 10px auto;">Dowiedz się, jak przygotować próbki do badań</a>
            </div>
            <div class="col-sm-6 col-md-6 hidden-xs" style="text-align: center;">
                <a class="btn btn-primary" role="button" href="#" data-toggle="modal" data-target="#modalinstrukcja" style="margin: 10px auto;">Dowiedz się, jak zarezerwować czas pomiarowy</a>
            </div>
            <div class="col-xs-12 visible-xs" style="text-align: center;">
                <a class="btn btn-primary btn-sm" role="button" href="#" data-toggle="modal" data-target="#modalprobki" style="margin: 10px auto;">Dowiedz się, jak przygotować próbki do badań</a>
            </div>
            <div class="col-xs-12 visible-xs" style="text-align: center;">
                <a class="btn btn-primary btn-sm" role="button" href="#" data-toggle="modal" data-target="#modalinstrukcja" style="margin: 10px auto;">Dowiedz się, jak zarezerwować czas pomiarowy</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-xs-12 col-sm-6">
        	<div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Spektroskopia EPR</h3>
                </div>
                <div class="panel-body">
                        <p style="text-align: justify;"><strong>Spektroskopia Elektronowego Rezonansu Paramagnetycznego (EPR)</strong> umożliwia badanie substancji zawierających centra paramagnetyczne. Próbka zawierająca niesparowane elektrony po umieszczeniu w stałym polu magnetycznym pochłania promieniowanie elektromagnetyczne o określonej częstości. Identyfikacja odbywa się na podstawie współczynnika rozszczepienia spektroskopowego (g), określającego siłę oddziaływania z zewnętrznym polem magnetycznym lub/i stałej struktury nadsubtelnej (A<span style="vertical-align: sub; font-size: small;">N</span>) określającej siłę oddziaływania z jądrem.</p>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Typy badanych układów:</h3>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">badania strukturalne jonów metali przejściowych</li>
                    <li class="list-group-item">układy elektron-dziura </li>
                    <li class="list-group-item">rodniki organiczne</li>
                </ul>
                <div class="panel-footer">Wszystkie pomiary wykonujemy w temperaturze pokojowej i ciekłego azotu.</div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Stosowane techniki pomiarowe:</h3>
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><strong>Spin-trapping</strong> - badanie rodników krótko żyjących (np. RFT) poprzez zastosowanie pułapek spinowych, które łatwo i wydajnie tworzą z rodnikami trwałe formy rodnikowe. Technika ta może posłużyć do badania mechanizmu powstawania stresu oksydacyjnego czy wykrywania i identyfikacji rodników powstających w skórze pod wpływem promieniowania.</li>
                    <li class="list-group-item"><strong>Spin labeling </strong> - technika pozwalająca badać strukturę i lokalną dynamikę białek. Stosowany znacznik spinowy łączy się z badaną cząsteczką za pomocą wielu wiązań i dają prosty sygnał EPR.</li>
                    <li class="list-group-item"><strong>Spin probing</strong> - zastosowanie sond spinowych stosuje się do badania dynamiki układów, kontrolowanego uwalniania leku, których widmo zależy od mikrolepkości i mikropolarności otoczenia, jak również od lokalnego pH.</li>
                    <li class="list-group-item"><strong>Radical scavenging</strong> – "zmiatanie wolnych rodników" służy do określenia właściwości antyutleniających w próbkach naturalnych takich jak: kawa, herbata, piwo, a także związków o potencjalnych własnościach antyrodnikowych.</li>
                    <li class="list-group-item"><strong>Pomiary ilościowe przy zastosowaniu różnych wzorców</strong> - podwójne całkowanie pozwala obliczyć pole powierzchni sygnału, które jest wprost proporcjonalne do stężenia centrów paramagnetycznych.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <div class="col-md-4">
                <h2>Wyposażenie</h2>
                <p>Na wyposażeniu laboratorium znajduje się spektrometr Bruker ELEXYS E500 CW-EPR oferujący częstotliwości mikrofalowe w paśmie X oraz Q.</p>
                <p><a class="btn btn-default" href="/wyposazenie" role="button">Więcej szczegółów &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Regulamin</h2>
                <p>Regulamin laboratorium elektronowego rezonansu paramagnetycznego zlokalizowanego na Wydziale Chemii Uniwersytetu Wrocławskiego.</p>
                <p><a class="btn btn-default" href="/regulamin" role="button">Więcej szczegółów &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h2>Kontakt</h2>
                <p>Dane kontaktowe i formularz do szybkiej komunikacji z zespołem badawczym. Chcesz się dowiedzieć jeszcze więcej? Napisz do nas!</p>
                <p><a class="btn btn-default" href="/kontakt" role="button">Więcej szczegółów &raquo;</a></p>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modalp1" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="width: 760px; margin-top: 80px;">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <img src="{{ URL::asset('img/info/spin_probe.png') }}" alt="EPR-1" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modalp2" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="width: 760px; margin-top: 80px;">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <img src="{{ URL::asset('img/info/widma_2.png') }}" alt="EPR-2" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="modalp3" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
        <div class="modal-dialog modal-lg" role="document" style="width: 760px; margin-top: 80px;">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <img src="{{ URL::asset('img/info/widmo_1.png') }}" alt="EPR-3" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modalprobki">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Sposób przygotowania i dostarczania próbek</h4>
                </div>
                <div class="modal-body" style="text-align: justify;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>Próbki przygotowane do pomiarów powinny znajdować się w odpowiednich probówkach lub kapilarach (kwarcowych, wyjątkowo plastikowych i szklanych), które mogą być niekiedy wypożyczone z Laboratorium.</p>
                        </div>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>PASMO</th>
                                <th>WARUNKI BADAŃ</th>
                                <th>WYMIARY PRÓBÓWEK [<span style="font-style: italic;">mm</span>]<br />(średnica zewnętrzna / wewnętrzna - długość)</th>
                            </tr>
                            <tr>
                                <td>X</td>
                                <td>temperatura pokojowa, proszki, roztwory<br />(rozpuszczalniki niepolarne)</td>
                                <td>5 / 3 - 190</td>
                            </tr>
                            <tr>
                                <td>X</td>
                                <td>roztwory temperatura pokojowa, roztwory<br />(rozpuszczalniki polarne)</td>
                                <td>kapilary  1,2 / 1 – 10</td>
                            </tr>
                            <tr>
                                <td>X</td>
                                <td colspan="2">dewar palcowy do ciekłego azotu: 5 / 3,8 - 190<br />probówki wewnętrzne (mogą być plastikowe): 3 / 2 – 30</td>
                            </tr>
                            <tr>
                                <td>Q</td>
                                <td colspan="2">probówki: 1.61 / 1.1 – 100 lub 190<br />kapilary: 0.4 / 0.3 – 100</td>
                            </tr>
                        </table>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">Probówki wypełniane są badanymi substancjami lub roztworami przez użytkowników. Zaleca się każdej próbce przypisać nazwę dla tworzonych plików komputerowych.</li>
                        <li class="list-group-item">Rurki pomiarowe nie mogą zawierać centrów paramagnetycznych. Zaleca się wykonać pomiar EPR pustej probówki, a także dewara z gilzą bibułową, jako tła badanego widma. Widmo tła należy odjąć od widma badanego.</li>
                        <li class="list-group-item">Probówki pomiarowe powinny być zamknięte, aby zapobiec wysypywaniu się proszków oraz skraplaniu się paramagnetycznego tlenu w próbkach zamrażanych w ciekłym azocie. Probówki pomiarowe muszą być czyste z zewnątrz, aby nie zanieczyścić wnęki rezonansowej.</li>
                        <li class="list-group-item">Próbki do pomiarów mogą mieć postać proszków, proszków rozcieńczonych diamagnetycznie, roztworów i roztworów zamrożonych.</li>
                        <li class="list-group-item">Roztwory zamrożone uzyskiwane są przez zanurzenie probówki z próbką  w ciekłym azocie w dewarze umieszczonym we wnęce rezonansowej lub przez przedmuchiwanie wnęki rezonansowej  gazowym azotem w odpowiedniej temperaturze (przystawka temperaturowa).</li>
                        <li class="list-group-item">Badania roztworów substancji paramagnetycznych w rozpuszczalnikach polarnych (woda, formamidy itp.) w temperaturze pokojowej mogą być wykonywane tylko dla próbek umieszczonych w bardzo cienkich płaskich probówkach lub kapilarach kwarcowych, ponieważ w tych rozpuszczalnikach następuje silna absorpcja promieniowania elektromagnetycznego w zakresie mikrofal. Zaleca się przedmuchiwanie próbek azotem.</li>
                        <li class="list-group-item">Do pomiarów wystarcza nawet niewielka ilość próbki (o wysokości nawet do kilku mm); zależy to od stężenia centrów paramagnetycznych.</li>
                    </ul>
                    Więcej na temat zasad obowiązujących w laboratorium przeczytasz w zakładce <a href="/regulamin">Regulamin</a>.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modalinstrukcja">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Rezerwacja czasu pomiarowego w laboratorium</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align: justify; font-style: italic;">
                        Wymaganymi wersjami przeglądarek do obsługi serwisu są IE (11), Edge (12), Firefox (12), Chrome (31), Safari (7.1), Opera (18) lub nowsze. Serwis działa również na najnowszych przeglądarkach mobilnych z wyłączeniem Opery Mini.
                    </p>
                    <p style="text-align: justify;">
                        Aby móc zarezerwować czas pomiarowy w laboratorium, należy zalogować się lub utworzyć swoje konto. W tym celu klikamy link <?php //reservationLink(); ?> znajdujący się na górnej belce nawigacyjnej. Osoby mające adres e-mail z domeny <span style="font-style: italic;">chem.uni.wroc.pl</span>, nie rejestrują się - przy pierwszym logowaniu będą poproszone o uzupełnienie swoich danych. Chcąc używać konta z adresem mailowym innym niż z domeny Wydziału Chemii, należy się zarejestrować, klikając na link <span style="font-style: italic;">Zarejestruj się</span>.
                    </p>
                    <p style="text-align: justify;">
                        Po zalogowaniu się na swoje konto w zakładce <span style="font-style: italic;">Rezerwacje</span> za pomocą kalendarza wybieramy datę badań oraz uzupełniamy najważniejsze szczegóły dotyczące ich zakresu. Aby wysłać zgłoszenie, klikamy przycisk <span style="font-style: italic;">Rezerwuj</span>.
                    <div style="text-align: center; margin-top: 10px;">
                        <img alt="rezerwacja czasu pomiarowego w laboratorium" src="img/reservation2.gif" style="max-width: 75%; border: solid #e7e7e7 2px; border-radius: 6px;" />
                    </div>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>

@endsection
