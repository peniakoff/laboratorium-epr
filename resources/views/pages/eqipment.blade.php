@extends('main')
@section('content')
    <div class="container">
      <div class="page-header">
        <h1>Wyposażenie</h1>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Wyposażenie laboratorium:</h3> </div>
            <ul class="list-group">
                <li class="list-group-item">spektrometr&nbsp;<a href="https://www.bruker.com/products/mr/epr/elexsys/e500/overview.html">Bruker ELEXSYS E500 CW-EPR</a></li>
                <li class="list-group-item">system kontroli temperatur (Bruker) ciągłego przepływu azotu w zakresie 100-400 K, Kriostaty ciągłego przepływu helu dla pasm X i Q oraz system kontroli temperatur (Oxford Instruments) w zakresie 4-400 K</li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Podstawowe dane techniczne spektrometrów:</h3>
              </div>
              <ul class="list-group">
                  <li class="list-group-item">częstości mikrofalowe: pasmo X (około 9.5 GHz) i pasmo Q (około 35 GHz)</li>
                  <li class="list-group-item">elektromagnesy: 0-1.5 Tesli, 0-0.7 Tesli</li>
                  <li class="list-group-item">mierniki dla pasma X [miernik pola (Teslametr ER 036TM typu NMR) i miernik częstości (E 41 FC)]</li>
                  <li class="list-group-item">rezonatory: pojedyncze i podwójne z oknem optycznym</li>
                  <li class="list-group-item">źródło UV do naświetlania in situ</li>
              </ul>
          </div>
        </div>
      <div class="col-xs-12 col-md-6" style="text-align:center;">
        <div class="panel panel-default">
          <img src="{{ URL::asset('img/Q_1.jpg') }}" alt="spektrometr działający w dwóch pasmach: X i Q" class="equip" />
          <div class="panel-footer">spektrometr działający w dwóch pasmach: X i Q</div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6" style="text-align:center;">
        <div class="panel panel-default">
          <img src="{{ URL::asset('img/X_1.jpg') }}" alt="spektrometr działający w paśmie X" class="equip" />
          <div class="panel-footer">spektrometr działający w paśmie X</div>
        </div>
      </div>
    </div>
@endsection
