@extends('main')
@section('content')
    <div class="container">
       <div class="page-header">
          <h1>Regulamin</h1>
       </div>
       <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading1">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                   1. Rodzaj pomiarów EPR  wykonywanych w laboratorium
                </h4>
             </div>
             <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                <div class="panel-body">
                   <ul>
                      <li>jakościowe  standardowe w paśmie X (9.5 GHz): w temperaturze pokojowej (~ 23°C) i w temp. ciekłego azotu ( ~ –200°C)</li>
                      <li>pomiary ilościowe z użyciem wzorców</li>
                      <li>pomiary z podłączeniem przystawki do uzyskiwania zmiennych temperatur w zakresie od -150°C do 100°C</li>
                      <li>pomiary niestandardowe w specjalnych warunkach: w paśmie Q (około 35 GHz) w temperaturze pokojowej;  w pasmach X i Q w temperaturach 4-300 K (dzięki zastosowaniu kriostatów ciągłego przepływu helu); z naświetlaniem promieniowaniem UV przez okno optyczne rezonatorów dla pasma X</li>
                   </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading2">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                   2. Czas pracy laboratorium
                </h4>
             </div>
             <div id="collapse2" class="panel-collapse collapse" role="tabpane2" aria-labelledby="heading2">
                <div class="panel-body">
                   <ul>
                      <li>Od godz. 7:30  do 15:30. Istnieje możliwość wykonywania  pomiarów w innych godzinach.</li>
                   </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading3">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                   3. Zasady użytkowania urządzeń znajdujących się w laboratorium
                </h4>
             </div>
             <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                <div class="panel-body">
                   <ul>
                       <li>Spektrometry EPR są obsługiwane przez operatora i osoby przeszkolone do wykonywania pomiarów. Terminy pomiarów ustala operator zgodnie z kolejnością zgłoszeń i specyfiką pomiarów.</li>
                   </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading4">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                   4. Uprawnienia i obowiązki operatora
                </h4>
             </div>
             <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                <div class="panel-body">
                    <ul>
                        <li>kalibracja spektrometru</li>
                        <li>wykonywanie pomiarów</li>
                        <li>ustalanie terminu pomiarów</li>
                        <li>powadzenie dokumentacji pomiarów, sporządzanie sprawozdań kwartalnych</li>
                        <li>zabezpieczenie laboratorium w odpowiednie materiały (kapilary, rurki pomiarowe (probówki), dewary, właściwe ciśnienie gazowego N2, zbiorniki z ciekłym  azotem oraz helem, podstawowe odczynniki, toner i  papier do drukarek, przenośne nośniki plików widm</li>
                        <li>bieżąca opieka nad aparaturą pomiarową i dbanie o sprawne działanie laboratorium</li>
                    </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading5">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                   5. Zasady szkolenia pracowników i doktorantów, zasady samodzielnego wykonywania pomiarów
                </h4>
             </div>
             <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                <div class="panel-body">
                   <ul>
                      <li>Szkolenie pracowników i  doktorantów do samodzielnego wykonywania pomiarów  EPR prowadzi operator spektometru lub osoba przez niego wyznaczona.</li>
                   </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading7">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                   6. Sposób rejestracji i ustalania kolejności wykonywania pomiarów
                </h4>
             </div>
             <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                <div class="panel-body">
                   <ul>
                       <li>Rejestracji daty pomiaru dokonuje się bezpośrednio na stronie internetowej laboratorium po zalogowaniu na indywidualne konto użytkownika. W szczególnych przypadkach możliwy jest także kontakt mailowy lub telefoniczny w celu uzgodnienia szczegółów badania.</li>
                   </ul>
                </div>
             </div>
          </div>
          <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="heading8">
                <h4 class="panel-title accordion" role="button" data-toggle="collapse" data-parent="#accordion" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                   7. Sposób odbioru danych pomiarowych
                </h4>
             </div>
             <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                <div class="panel-body">
                   <ul>
                      <li>Dane pomiarowe (widma EPR) zapisywane są na serwerze i udostępniane na indywidualnym koncie użytkownika w zakładce <span style="font-style: italic;">Pliki</span>. W wyjątkowych sytuacjach mogą być odbierane w formie elektronicznej lub papierowej w pracowni.</li>
                   </ul>
                </div>
             </div>
          </div>
       </div>
    </div>
@endsection
