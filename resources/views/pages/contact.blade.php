@extends('main')
@section('content')
    <div class="container">
        <?php /*
        if (isset($_SESSION['send_mail'])) {
            echo ('<div class="alert alert-success alert-dismissible" role="alert" style="margin-top: 30px; margin-bottom: -10px;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Dziękujemy za kontakt!</strong> Wiadomość została wysłana. Odpowiemy najszybciej, jak to możliwe.</div>');
            unset($_SESSION['send_mail']);
        } */
        ?>
        <div class="page-header">
            <h1>Kontakt</h1>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-4">
                Laboratorium EPR
                <br />Wydział Chemii Uniwersytetu Wrocławskiego
                <br /> ul. F. Joliot-Curie 14
                <br />50-383 Wrocław
                <br />tel. +48 71 375 7128
                <br />
                <a href="http://www.google.com/recaptcha/mailhide/d?k=018hX-mwFsbJzbKeiximPQOA==&amp;c=glTDWEmRcJccZlBjNDVGKcFTXT-Nezhy_xGpLrU-w6E=" onclick="window.open('http://www.google.com/recaptcha/mailhide/d?k\x3d018hX-mwFsbJzbKeiximPQOA\x3d\x3d\x26c\x3dglTDWEmRcJccZlBjNDVGKcFTXT-Nezhy_xGpLrU-w6E\x3d', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;" title="Reveal this e-mail address"><span class="reverse">lp.corw.inu.mehc@rpe</span></a>
            </div>
            <div class="col-xs-6 col-md-4">
                <h4>Opiekun naukowy</h4>
                <a href="http://faculty.chem.uni.wroc.pl/pracownik/AdamJezierski/">prof. dr hab. Adam Jezierski</a><br />
                tel. +48 71 375 7330
            </div>
            <div class="col-xs-6 col-md-4">
                <h4>Operator spektometru</h4>
                <a href="http://faculty.chem.uni.wroc.pl/pracownik/AgnieszkaLewinska/">dr Agnieszka Lewińska</a><br />
                tel. +48 71 375 7324
            </div>
        </div>
        <h2 style="margin: 30px auto 20px;">Formularz kontaktowy</h2>
        <form class="form-horizontal" method="post">
            <?php /*
            if (isset($_SESSION['e_name'])) {
                echo ('<div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    '.$_SESSION["e_name"].'</div>');
                unset($_SESSION['e_name']);
            } */
            ?>
            <div class="form-group">
                <label class="control-label col-xs-6 col-sm-3 col-md-2">Imię i nazwisko</label>
                <div class="col-xs-10 col-sm-7">
                    <?php //setName(); ?>
                </div>
            </div>
            <?php /*
            if (isset($_SESSION['e_email'])) {
                echo ('<div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    '.$_SESSION["e_email"].'</div>');
                unset($_SESSION['e_email']);
            } */
            ?>
            <div class="form-group">
                <label class="control-label col-xs-6 col-sm-3 col-md-2">Adres e-mail</label>
                <div class="col-xs-10 col-sm-7">
                    <?php //setMail(); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-6 col-sm-3 col-md-2">Wiadomość</label>
                <div class="col-xs-10 col-sm-7">
                    <textarea class="form-control" name="email_text" rows="4"><?php //setText(); ?></textarea>
                </div>
            </div>
            <?php /*
            if (isset($_SESSION['e_robot'])) {
                echo ('<div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    '.$_SESSION["e_robot"].'</div>');
                unset($_SESSION['e_robot']);
            } */
            ?>
            <div class="form-group">
                <label class="control-label col-xs-6 col-sm-3 col-md-2"></label>
                <div class="col-xs-10 col-sm-7">
                    <?php /*
                    if (!isset($_SESSION['logged'])) echo ('<div class="g-recaptcha" data-sitekey="6Lce1CMTAAAAAN9X7RyoEafOpPJjkJVGlxRcgqfk" style="margin-bottom: 10px;"></div>'); */
                    ?>
                    <input class="btn btn-primary" type="submit" value="Wyślij">
                </div>
            </div>
        </form>
    </div>
@endsection
